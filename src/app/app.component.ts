import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { MenuController } from '@ionic/angular';



@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  navigate : any;

  unsubscribeBackEvent:any;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private menu: MenuController
  ) {
    this.sideMenu();

    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
  sideMenu()
  {
    this.navigate =
    [
      {
        title : "Accounts",
        url   : "/profile",
        icon  : "home-outline"
      },
      {
        title : "Language",
        url   : "/language",
        icon  : "language-outline"
      },
      {
        title : "Settings",
        url   : "/changepass",
        icon  : "settings-outline"
      },
      {
        title : "Logout",
        url   : "/login",
        icon  : "log-out-outline"
      }
    ]
  }

  initializeBackButtonCustomHandler(): void {
    this.unsubscribeBackEvent = this.platform.backButton.subscribeWithPriority(
      0,
      async () => {
        try {
          const element = await this.menu.getOpen();
          if (element) {
            this.menu.close();
            return;
          } else {
            navigator["login"].exitApp();
          }
        } catch (error) {}
      }
    );
  }
  //function used to handle harware back button event end
}
