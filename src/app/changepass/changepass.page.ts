import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators
} from "@angular/forms";

import {ServiceapiService} from '../serviceapi.service'
import { from } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-changepass',
  templateUrl: './changepass.page.html',
  styleUrls: ['./changepass.page.scss'],
})
export class ChangepassPage implements OnInit {

  changepassForm:FormGroup;
  oldshoweye: boolean = true;
  showeye: boolean = true;
  cshoweye: boolean = true;
  password_check: boolean = false;
  passwordType: string = "password";
  cpasswordType: string = "password";
  oldpasswordType: string = "password";
  error_messages = {
    
    oldpass: [
      //message for password
      { type: "required", message: "Password is required" },
      {
        type: "minlength",
        message: "Password length must longer or equal to 3 characters"
      },
      {
        type: "maxlength",
        message: "Password length must be lower or equal to 20 characters"
      },
      {
        type: "pattern",
        message: "Password should contain 1 uppercase, 1 lowercase"
      }
    ],
    newpasswod: [
      //message for password
      { type: "required", message: "Password is required" },
      {
        type: "minlength",
        message: "Password length must longer or equal to 3 characters"
      },
      {
        type: "maxlength",
        message: "Password length must be lower or equal to 20 characters"
      },
      {
        type: "pattern",
        message: "Password should contain 1 uppercase, 1 lowercase"
      }
    ]
  };

  constructor(
    public formBuilder: FormBuilder,
    private serviceapiService:ServiceapiService,
    private route:Router
  ) {
    this.changepassForm = this.formBuilder.group({
      oldpass: new FormControl(
        "",
        Validators.compose([
          //fetch value from userid field and validate it
          Validators.required, // check empty field
          Validators.minLength(3), // check minimum length
          Validators.maxLength(30), //check maximum length
          Validators.pattern("^(?=.*[0-9])[A-za-z0-9!@#$%^&*]{8,16}$") //RegEx for password
        ])
      ),
      newpasswod: new FormControl(
        "",
        Validators.compose([
          //fetch value from userid field and validate it
          Validators.required, // check empty field
          Validators.minLength(3), // check minimum length
          Validators.maxLength(30), //check maximum length
          Validators.pattern("^(?=.*[0-9])[A-za-z0-9!@#$%^&*]{8,16}$") //RegEx for password
        ])
      )
      ,
      cnfpass: new FormControl(
        "",
        Validators.compose([
          //fetch value from userid field and validate it
          Validators.required // check empty field
        ])
      )
    });
   }

  ngOnInit() {
  }

    // show form validating error message start
    errorToast(flag) {
      for (let error of this.error_messages[flag]) {
        if (
          this.changepassForm.get(flag).hasError(error.type) &&
          (this.changepassForm.get(flag).dirty || this.changepassForm.get(flag).touched)
        ) {
          // check data present in input field is valid or not
          this.serviceapiService.presentToast(error.message); // show toast
        }
      }
    }
    // show form validating error message end

    checkPasswords() {
      // here we have the 'passwords' group
      let pass = this.changepassForm.get("newpasswod").value;
      let confirmPass = this.changepassForm.get("cnfpass").value;
      if (pass != confirmPass) {
        this.serviceapiService.presentToast("Password not matched"); // show toast
      } else {
        this.password_check = true;
      }
    }
  
    passwordhideeye() {
      //show and hide password
      this.showeye = !this.showeye;
      this.passwordType = this.passwordType == "text" ? "password" : "text";
    }
    cpasswordhideeye() {
      //show and hide confirm password
      this.cshoweye = !this.cshoweye;
      this.cpasswordType = this.cpasswordType == "text" ? "password" : "text";
    }
    oldpasswordhideeye() {
      //show and old password
      this.oldshoweye = !this.oldshoweye;
      this.oldpasswordType = this.oldpasswordType == "text" ? "password" : "text";
    }

    resetPassword(){
      this.serviceapiService.databaseVar.executeSql(`
      UPDATE register
      SET password = '${this.changepassForm.get('newpasswod').value}'
      WHERE email = '${this.serviceapiService.usermail}'
    `, [])
      .then(() => {
        console.log("password updated");
        this.route.navigate(['dashboard']);
      })
      .catch(e => {
        // alert("error " + JSON.stringify(e))
        console.log(JSON.stringify(e));
        
      });
    }
}
