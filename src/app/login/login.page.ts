import { Component, OnInit } from '@angular/core';

import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators
} from "@angular/forms";
import { Router } from "@angular/router";

import {ServiceapiService} from '../serviceapi.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loginForm: FormGroup;
  public userEmail: string;
  public userPassword: string;
  public loadingElement: any;
  public unsubscribeBackEvent: any;
  showeye: boolean = true;
  passwordType: string = "password";
  showFooter = false;
  style: boolean = true;
  checkDevice: boolean = false;
  //create error message for email and password start
  error_messages = {
    email: [
      // message for email
      { type: "required", message: "Email is required" },
      {
        type: "minlength",
        message: "Email length must longer or equal to 3 characters"
      },
      {
        type: "maxlength",
        message: "Email length must be lower or equal to 20 characters"
      },
      { type: "pattern", message: "Please enter a valid Email" }
    ],
    password: [
      //message for password
      { type: "required", message: "Password is required" },
      {
        type: "minlength",
        message: "Password length must longer or equal to 3 characters"
      },
      {
        type: "maxlength",
        message: "Password length must be lower or equal to 20 characters"
      },
      {
        type: "pattern",
        message: "Password should contain 1 uppercase, 1 lowercase"
      }
    ]
  };
  //create error message for email and password end
  constructor(
    private route: Router,
    public formBuilder: FormBuilder,
    private serviceapiService:ServiceapiService
  ) { 
    this.loginForm = this.formBuilder.group({
      password: new FormControl(
        "",
        Validators.compose([
          //fetch value from userid field and validate it
          Validators.required, // check empty field
          Validators.minLength(3), // check minimum length
          Validators.maxLength(30), //check maximum length
          Validators.pattern("^(?=.*[0-9])[A-za-z0-9!@#$%^&*]{8,16}$") //RegEx for password
        ])
      ),
      email: new FormControl(
        "",
        Validators.compose([
          //fetch value from password feild and validate it
          Validators.required, //check empty field
          Validators.minLength(3), // check minimum length
          Validators.maxLength(30), //check maximum length
          Validators.pattern(
            "^[A-Za-z]{2,}[A-Za-z0-9]{0,}[.]{0,1}[A-Za-z0-9]{1,}[.]{0,1}[A-Za-z0-9]{1,}@[A-Za-z]{2,}[.]{1}[A-za-z]{2,3}[.]{0,1}[a-z]{0,2}$"
          ) //RegEx for email
        ])
      )
    });
  }

  ngOnInit() {
  }
  gotoregister(){
    this.route.navigate(['register']);
  }

  // show form validating error message start
  errorToast(flag) {
    for (let error of this.error_messages[flag]) {
      if (
        this.loginForm.get(flag).hasError(error.type) &&
        (this.loginForm.get(flag).dirty || this.loginForm.get(flag).touched)
      ) {
        // check data present in input field is valid or not
        this.serviceapiService.presentToast(error.message); // show toast
      }
    }
  }
  // show form validating error message end

  passwordhideeye() {
    //show and hide password
    this.showeye = !this.showeye;
    this.passwordType = this.passwordType == "text" ? "password" : "text";
  }
  user_login(){
console.log("user function start");

    this.serviceapiService.databaseVar.executeSql(`
    SELECT * FROM register WHERE  email='${this.loginForm.get('email').value}' AND password='${this.loginForm.get('password').value}'`
      , [])
      .then((res) => {
        //this.row_data = [];
        debugger;
        if (res.rows.length > 0) {
          this.serviceapiService.usermail=this.loginForm.get('email').value;
          this.route.navigate(['dashboard']);
        }
        else{
          this.serviceapiService.presentToast("Enter correct Credentials")
        }
      })
      .catch(e => {
        debugger;
        alert("error " + JSON.stringify(e))
      });
    }

}
