import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators
} from "@angular/forms";
// import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Platform } from "@ionic/angular";
import {ServiceapiService} from '../serviceapi.service'
import { from } from 'rxjs';
import { SQLiteObject, SQLite } from '@ionic-native/sqlite/ngx';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  public gender;
  registerForm: FormGroup;
  public userFirstname: string;
  public userLastname: string;
  public userEmail: string;
  public userPassword: string;
  public userCpassword: string;
  public mNumber: string;
  public radioModel;
  public unsubscribeBackEvent: any;
  passwordType: string = "password";
  cpasswordType: string = "password";
  showeye: boolean = true;
  cshoweye: boolean = true;
  checkDevice: boolean = false;

  databaseObj: SQLiteObject;
  readonly database_name: string = "userData.db";  //db name
  readonly table_name: string = "register";        //table name

  //Error message while entering data by user
  error_message = {
    //error message for firstname field
    fname: [
      { type: "required", message: "compulsory field" },
      {
        type: "minlength",
        message: "Name length must longer or equal to 3 characters"
      },
      {
        type: "maxlength",
        message: "Name length must lower or equal to 12 characters"
      },
      { type: "pattern", message: "Only character are allow" }
    ],
    //errot messafe for last name field
    lastname: [
      { type: "required", message: "compulsory field" },
      {
        type: "minlength",
        message: "Name length must longer or equal to 3 characters"
      },
      {
        type: "maxlength",
        message: "Name length must lower or equal to 12 characters"
      },
      { type: "pattern", message: "Only character are allow" }
    ],
    //Error message for email
    email: [
      { type: "required", message: "compulsory field" },
      { type: "pattern", message: "Not a valid email id" }
    ],
    //Error message for password
    password: [
      { type: "required", message: "compulsory field" },
      {
        type: "minlength",
        message: "Password length must longer or equal to 8 characters"
      },
      {
        type: "maxlength",
        message: "Password length must lower or equal to 20 characters"
      },
      {
        type: "pattern",
        message: "Password should contain alphabets and number"
      }
    ],
    //Error message for Confirm password
    confirm_password: [
      { type: "required", message: "compulsory field" },
      {
        type: "notEquivalent",
        message: "Confirm password not match with password"
      }
    ],
    //Error message for mobile number
    mobilenumber: [
      { type: "required", message: "compulsory field" },
      { type: "minlength", message: "Should contain 10 number only" },
      { type: "maxlength", message: "Should contain 10 number only" },
      { type: "pattern", message: "Not a valid number" }
    ],
    //Error message gender
    gender: [{ type: "required", message: "choose atleast one" }],
    //error message for check box to select term and condition
    checkbox: [{ type: "required", message: "click on term & condition" }]
  };
  constructor(
    public formBuilder: FormBuilder,
    private serviceapiService:ServiceapiService,
    //public http: HttpClient,
    private router: Router,
    private sqlite: SQLite,
    private platform:Platform

  ) {

    this.platform.ready().then(() => {
      this.createDB();
    }).catch(error => {
      console.log(error);
    })



    this.registerForm = this.formBuilder.group({
      fname: new FormControl(
        "",
        Validators.compose([
          //fetch value from userid field and validate it
          Validators.required, // check empty field
          Validators.minLength(3), // check minimum length
          Validators.maxLength(12), //check maximum length
          Validators.pattern("^[A-za-z]{2,}$") //RegEx for name
        ])
      ),
      lastname: new FormControl(
        "",
        Validators.compose([
          //fetch value from userid field and validate it
          Validators.required, // check empty field
          Validators.minLength(3), // check minimum length
          Validators.maxLength(12), //check maximum length
          Validators.pattern("^[A-za-z]{2,}$") //RegEx for name
        ])
      ),
      email: new FormControl(
        "",
        Validators.compose([
          //fetch value from password feild and validate it
          Validators.required, //check empty field
          Validators.pattern(
            "^[A-Za-z]{2,}[A-Za-z0-9]{0,}[.]{0,1}[A-Za-z0-9]{1,}[.]{0,1}[A-Za-z0-9]{1,}@[A-Za-z]{2,}[.]{1}[A-za-z]{2,3}[.]{0,1}[a-z]{0,2}$"
          ) //RegEx for email
        ])
      ),
      password: new FormControl(
        "",
        Validators.compose([
          //fetch value from userid field and validate it
          Validators.required, // check empty field
          Validators.minLength(8), // check minimum length
          Validators.maxLength(20), //check maximum length
          Validators.pattern("^(?=.*[0-9])[A-za-z0-9!@#$%^&*]{8,16}$") //RegEx for password
        ])
      ),
      confirm_password: new FormControl(
        "",
        Validators.compose([
          //fetch value from userid field and validate it
          Validators.required, // check empty field
          Validators.minLength(8), // check minimum length
          Validators.maxLength(20), //check maximum length
          Validators.pattern("^(?=.*[0-9])[A-za-z0-9!@#$%^&*]{8,16}$") //RegEx for password
        ])
      ),
      mobilenumber: new FormControl(
        "",
        Validators.compose([
          //fetch value from userid field and validate it
          Validators.required, // check empty field
          Validators.minLength(10), // check minimum length
          Validators.maxLength(10), //check maximum length
          Validators.pattern("^[789][0-9]{9}$") //RegEx for mobile number
        ])
      ),
      gender: new FormControl(
        "",
        Validators.compose([
          //fetch value from userid field and validate it
          Validators.required // check empty field
        ])
      )
    });
   }

  ngOnInit() {
  }
  errorToast(flag) {
    // validation error message
    for (let error of this.error_message[flag]) {
      if (
        this.registerForm.get(flag).hasError(error.type) &&
        (this.registerForm.get(flag).dirty ||
          this.registerForm.get(flag).touched)
      ) {
        this.serviceapiService.presentToast(error.message); // show error message in toast
      }
    }
  }

  checkPasswords() {
    // here we have the 'passwords' group
    let pass = this.registerForm.get("password").value;
    let confirmPass = this.registerForm.get("confirm_password").value;
    if (pass != confirmPass) {
      this.serviceapiService.presentToast(
        "Password and confirm password are not matched"
      ); // show toast
    }
  }

   //function for getting value from gender
   radio(event) {
    // check radio event
    return this.radioModel;
  }

  // Create DB if not there
  createDB() {
    this.sqlite.create({
      name: this.database_name,
      location: 'default'
    })
    .then((db: SQLiteObject) => {
        debugger;
        this.serviceapiService.databaseVar = db;
        this.createTable();
      })
      .catch(e => {
        alert("error " + JSON.stringify(e))
      });

      
  }
  // Create table
  createTable() {
    this.serviceapiService.databaseVar.executeSql(
      `CREATE TABLE IF NOT EXISTS register  (firstName text, lastName text,email text PRIMARY KEY,password text, mobileNumber INT,gender text) `, [])
      .then(() => {
      })
      .catch(e => {
      });
  }
  register(){
    this.serviceapiService.databaseVar.executeSql(`
      INSERT INTO register (firstName,lastName,email,password,mobileNumber,gender) VALUES ('${this.registerForm.get('fname').value}','${this.registerForm.get('lastname').value}','${this.registerForm.get('email').value}','${this.registerForm.get('password').value}',${this.registerForm.get('mobilenumber').value},'${this.registerForm.get('gender').value}')
    `, [])
      .then(() => {
        //debugger;
        console.log(' Inserted!');
        this.serviceapiService.presentToast("User Register Successfully")
        this.router.navigate(['login']);
      })
      .catch(e => {
        //alert("error " + JSON.stringify(e))
        console.log("user register error",JSON.stringify(e));
        this.serviceapiService.presentToast("Registration Fail!!!");
      });

  }
}
