import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ServiceapiService {

  databaseVar;
  usermail;
  constructor(
    private toastController:ToastController
  ) { }
  async presentToast(msg) {// used to show message in toast
    const toast = await this.toastController.create({
      message: msg,//toast message
      position: 'top',//toast position
      animated: true,
      color: "dark",// toast color
      duration: 1500 //time duration to display 
    });
    toast.present();
  }

  
}
